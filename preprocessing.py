import os
import shutil
import pandas as pd

train = pd.read_csv("trainLabels.csv")

for label in train.label.unique():
    if(not os.path.isdir('./train/' + label)):
        os.mkdir('./train/' + label)
    if(not os.path.isdir('./validation/' + label)):
        os.mkdir('./validation/' + label)
    i = 0
    for names in os.listdir('./train/' + label):
        i += 1
        shutil.move('./train/' + label + '/' + names, './validation/' + label)
        if(i >= 500):
            break

for index, row in train.iterrows():
    shutil.move('./train/' + str(row['id']) + '.png', './train/' + row['label'])
    print(str(row['id']) + ' ' + row['label'])



